source("plotCommon.r")


read_graph<-function(filename,  name) {
x<-read.table(paste(dir, "res-", filename, ".txt", sep=""), header=TRUE)
x$prob <- name;
return (x);
}

l_chol<- read_graph("Cholesky", "Cholesky")
l_qr <- read_graph("QR-ib128", "QR")
l_lu <- read_graph("LU-nopiv-ib96", "LU")

xbb<-rbind(l_chol, l_qr, l_lu)

# Cope with NAs in the input
'%==%' <- function(a, b) a==b & !is.na(a);

xbb$algName<-as.character(xbb$algorithm)
xbb$algName[xbb$algorithm == "HetPrio" & xbb$ssbaf %==% "yes"] <- "HetPrioDep"
xbb[xbb$algorithm == "HetPrio" & xbb$stealLatest %==% "yes", "algName"] <- "HetPrioIndep"
xbb[xbb$algorithm == "zhang", "algName" ] <- "OnlineZhang"
xbb[xbb$algorithm == "indep", "algName"] <- as.character(xbb[xbb$algorithm == "indep", "indep"])
xbb[xbb$algorithm == "depbound", "algName"] <- "bound"
xbb$algName[xbb$algorithm == "greedy" & xbb$stealer %==% 1] <- "AreaListSteal"
xbb$algName[xbb$algorithm == "greedy" & xbb$stealer %==% -1] <- "AreaList"

xbb<-ddply(xbb, ~input+prob, transform, ratio=mkspan/mkspan[algName == "bound"])

levels(xbb$rank) <- c(levels(xbb$rank), "None")
xbb$rank[is.na(xbb$rank)] <- "None"

                                     # Computations for acceleration factors and waste plots


xbb$waste_CPU<-xbb$mkspan*nb_cpu-xbb$CPU.used
xbb$waste_GPU<-xbb$mkspan*nb_gpu-xbb$GPU.used
xbb<-ddply(xbb, ~input+prob, transform, waste_CPU_ratio=waste_CPU/(nb_cpu*mkspan[algName=="bound"]), 
           waste_GPU_ratio=waste_GPU/(nb_gpu*mkspan[algName=="bound"]))


xw<-melt(xbb[! xbb$algName %in% c("bound", "depbound"),], id.vars=c("input", "algName", "prob", "rank"), measure.vars=c("waste_CPU_ratio", "waste_GPU_ratio"))
xw$variable<-factor(xw$variable, levels=c("waste_GPU_ratio", "waste_CPU_ratio"), labels=c("GPU", "CPU"))




prepare_plot<-function(p) {
p <- p + theme(legend.position="bottom")
p <- p + xlab("Number of tiles")
p <- p + scale_x_continuous(breaks=seq(8, 64, 8), minor_breaks=seq(4, 64, 4))
return(p)
}

algNames <- function(p) {
# p <- p + scale_color_discrete(name="Algorithm", breaks=c("AreaList", "AreaListSteal", "balest", "balmks", "dualhp", "heft", "HetPrio", "zhang", "OnlineZhang"),
#                               labels=c("AreaList", "AreaListSteal", "BalancedEst", "BalancedMakespan", "DualHP", "HEFT", "HeteroPrio", "Al5 from [21] (batch)", "Al5 (online)"))
p <- p + scale_color_manual(name="Algorithm", breaks=c("AreaList", "AreaListSteal", "balest", "balmks", "dualhp", "heft", "HetPrio", "zhang", "OnlineZhang", "bound"),
                              labels=c("AreaList", "AreaListSteal", "BalancedEst", "BalancedMakespan", "DualHP", "HEFT", "HeteroPrio", "CYZ-5 (batch)", "CYZ-5 (online)", "Lower bound"),
			      values=colors)
p <- p + scale_linetype_manual(name="Algorithm", breaks=c("AreaList", "AreaListSteal", "balest", "balmks", "dualhp", "heft", "HetPrio", "zhang", "OnlineZhang", "bound"),
                              labels=c("AreaList", "AreaListSteal", "BalancedEst", "BalancedMakespan", "DualHP", "HEFT", "HeteroPrio", "CYZ-5 (batch)", "CYZ-5 (online)", "Lower bound"),
			      values=linetypes)

p <- p + guides(color=guide_legend(nrow=3,byrow=TRUE, keywidth=3))

}



xbbBest <- ddply(xbb, ~input+prob+algName, summarize,
                 waste_CPU_ratio = waste_CPU_ratio[which.min(ratio)],
                 waste_GPU_ratio = waste_GPU_ratio[which.min(ratio)],
                 CPU.af = CPU.af[which.min(ratio)],
                 GPU.af = GPU.af[which.min(ratio)],
                 ratio=min(ratio) )

xaf<-melt(xbbBest[! xbbBest$algName %in% c("bound", "depbound", "HetPrioDep", "HetPrioIndep"),], id.vars=c("algName", "input", "prob"), measure.vars=c("GPU.af", "CPU.af"))
xaf$variable<-factor(xaf$variable, levels=c("GPU.af", "CPU.af"), labels=c("GPU", "CPU"))
## xaf$value <- as.numeric(xaf$value)

xw<-melt(xbbBest[! xbbBest$algName %in% c("bound", "depbound", "HetPrioDep", "HetPrioIndep"),], id.vars=c("input", "algName", "prob"), measure.vars=c("waste_CPU_ratio", "waste_GPU_ratio"))
xw$variable<-factor(xw$variable, levels=c("waste_GPU_ratio", "waste_CPU_ratio"), labels=c("GPU", "CPU"))




xbbAvg <- ddply(xbb, ~input+algName, summarize, time = mean(time))

## p<-ggplot(xbb[xbb$alg != "bound",], aes(x=N, y=ratio, color=alg, linetype=rank)) + geom_line() + facet_grid(prob~., scales="free")
## p <- prepare_plot(p)
## p <- p + ylab("Ratio of makespan to lower bound")
## ggsave("All-Ratio.pdf",  p, height=9, width=6)

p<-ggplot(xbbBest[! xbbBest$algName %in% c("bound", "depbound", "HetPrioDep", "HetPrioIndep"),], aes(x=input, y=ratio, color=algName, linetype=algName)) + geom_line() + facet_grid(prob~., scales="free")
p <- prepare_plot(p)
 p <- algNames(p)
p <- p + ylab("Ratio of makespan to lower bound")
ggsave(paste(outdir, "All-Ratio-Summarized.pdf", sep=''),  p, height=9, width=7)

p<-ggplot(xbbAvg[! xbbAvg$algName %in% c("depbound", "HetPrioDep", "HetPrioIndep"),], aes(x=input, y=time/1000, color=algName, linetype=algName)) + geom_line()
p <- prepare_plot(p)
p <- algNames(p)
p <- p + scale_y_log10()
p <- p + ylab("Computation time (s, logscale)")
ggsave(paste(outdir, "All-Time.pdf", sep=''),  p, height=6, width=6)

p <- ggplot(xbb[xbb$algorithm == "HetPrio",], aes(x=input, y=ratio, shape=algName, color=algName, linetype=rank)) + geom_line() + geom_point() + facet_grid(prob~., scales="free")
p <- prepare_plot(p)
p <- p + theme(legend.box="horizontal")
p <- p + scale_color_discrete(name="Algorithm",
                              breaks=c("HetPrio", "HetPrioDep", "HetPrioIndep"),
                              labels=c("HeteroPrio", "HeteroPrioDep", "HeteroPrioIndep"))
p <- p + scale_shape_discrete(name="Algorithm",
                              breaks=c("HetPrio", "HetPrioDep", "HetPrioIndep"),
                              labels=c("HeteroPrio", "HeteroPrioDep", "HeteroPrioIndep"))
p <- p + guides(color=guide_legend(nrow=2,byrow=TRUE),
                linetype=guide_legend(nrow=2, byrow=TRUE))
p <- p + scale_linetype_discrete(name="Ranking", breaks=c("heft", "min", "fifo", "area"), labels=c("avg", "min", "fifo", "area"))
p <-  p + ylab("Ratio of makespan to lower bound")
ggsave(paste(outdir, "HetPrio-Comparison.pdf", sep=''), p, height=6, width=6)



p<-ggplot(xaf, aes(x=input, y=value, color=algName, linetype=algName)) + geom_line() + facet_grid(prob~variable, scales="free_y")
p <- prepare_plot(p)
p <- algNames(p)
p <- p + ylab("Avg acceleration ratio")
ggsave(paste(outdir, "All-Accel.pdf", sep=''), p, height=6, width=7)

p<-ggplot(xw, aes(x=input, y=value, color=algName, linetype=algName)) + geom_line() + facet_grid(variable~prob, scales="free_y")
p <- prepare_plot(p)
p <- algNames(p)
p <- p + ylab("Ratio of waste to area bound")
ggsave(paste(outdir, "All-Waste.pdf", sep=''), p, height=5, width=7)

