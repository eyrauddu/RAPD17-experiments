#!/bin/bash

pmtool=${PMTOOL_PATH}/pmtool

graphs=./inputs/
output=./data/

for graphDir in $graphs/*; do
    name=$(basename $graphDir);
    echo $name
    rm -f ${output}/res-${name}-Indep.txt
    $pmtool $graphDir/taskGraph*-4.txt --no-dep -b area:hybrid=no -a hetprio -a hetprio:stealLatest=yes -a heft -a indep:indep=dualhp -a indep:indep=balest,balmks:dosort=no  -a zhang -a indep:indep=zhang:sortby=avg,min,accel,none:rev=yes,no --threads=2 | sed -u -e "s/^.*-\([0-9]*\)\.txt/\1/" > ${output}/res-${name}-Indep.txt
    for n in $(seq 8 4 40); do 
	$pmtool $graphDir/taskGraph*-${n}.txt --no-header --no-dep -b area:hybrid=no -a hetprio -a hetprio:stealLatest=yes -a heft -a indep:indep=dualhp -a indep:indep=balest,balmks:dosort=no  -a zhang -a indep:indep=zhang:sortby=avg,min,accel,none:rev=yes,no --threads=2 | sed -u -e "s/^.*-\([0-9]*\)\.txt/\1/" >> ${output}/res-${name}-Indep.txt
    done
done


