#!/bin/bash

pmtool=${PMTOOL_PATH}/pmtool

graphs=./inputs/
output=./data/

for graphDir in $graphs/*; do
    name=$(basename $graphDir);
    echo $name
    rm -f ${output}/res-${name}.txt
    $pmtool $graphDir/taskGraph*.txt -a hetprio:rank=heft,min,area -a hetprio:stealLatest=yes:rank=min,heft,area -a hetprio:stealidle=no:ssbaf=yes:rank=min,heft,area -a heft:rank=heft,min,area -a indep:rank=heft,min,fifo,area:indep=dualhp -a indep:indep=balest,balmks:dosort=no -a indep:indep=balest,balmks:rank=area,heft,min,fifo -b dep:share=dep  -a greedy:file=int@dep[alloc]:stealer=-1,1 -a zhang:rank=area,min,heft,none,fifo:stealer=1,-1 -a indep:indep=zhang:sortby=accel,avg,min,none:rev=yes,no:rank=min:style=onprogress:proportion=0.1  -r ${output}/rep-${name}.txt --threads=2 | sed -u -e "s/^.*-\([0-9]*\)\.txt/\1/" >> ${output}/res-${name}.txt
done
