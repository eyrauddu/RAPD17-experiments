source("plotCommon.r")

read_graph<-function(filename, name) {
   x <- read.table(paste(dir, "res-", filename, "-Indep.txt", sep=""), header=TRUE, na.strings="")
   x$prob<-name
   return(x)
}

prepare_plot<-function(p) {
p <- p + theme(legend.position="bottom")
# p <- p + scale_color_discrete(name="Algorithm",
#                               breaks=c("balest", "balmks", "dualhp", "heft", "HetPrio", "HetPrioIndep", "zhang", "OnlineZhang", "bound" ),
#                               labels=c("BalancedEst", "BalancedMakespan", "DualHP", "HEFT", "HeteroPrio", "HeteroPrioIndep", "CYZ-5", "Al5 (online)", "Area Bound"))
#  p <- p + scale_linetype_discrete(name="Algorithm",
#                                breaks=c("balest", "balmks", "dualhp", "heft", "HetPrio", "HetPrioIndep", "zhang", "OnlineZhang", "bound"),
#                                labels=c("BalancedEst", "BalancedMakespan", "DualHP", "HEFT", "HeteroPrio", "HeteroPrioIndep", "CYZ-5", "Al5 (online)", "Area Bound"))
p <- p + scale_color_manual(name="Algorithm",
                             breaks=c("balest", "balmks", "dualhp", "heft", "HetPrio", "HetPrioIndep", "zhang", "OnlineZhang", "bound" ),
                             labels=c("BalancedEst", "BalancedMakespan", "DualHP", "HEFT", "HeteroPrio", "HeteroPrioIndep", "CYZ-5", "Al5 (online)", "Area Bound"), 
			      values = colors)
p <- p + scale_linetype_manual(name="Algorithm",
                              breaks=c("balest", "balmks", "dualhp", "heft", "HetPrio", "HetPrioIndep", "zhang", "OnlineZhang", "bound"),
                              labels=c("BalancedEst", "BalancedMakespan", "DualHP", "HEFT", "HeteroPrio", "HeteroPrioIndep", "CYZ-5", "Al5 (online)", "Area Bound"), 
			      values = linetypes)
p <- p + guides(color=guide_legend(nrow=2, byrow=TRUE,keywidth=2))
p <- p + xlab("Number of tiles")
p <- p + scale_x_continuous(breaks=seq(8, 64, 8), minor_breaks=seq(4, 64, 4))
return(p)
}

l_chol<- read_graph("Cholesky", "Cholesky")
l_qr <- read_graph("QR-ib128", "QR")
l_lu <- read_graph("LU-nopiv-ib96", "LU")

x<-rbind(l_chol, l_qr, l_lu)
xbb<-ddply(x, ~input+prob, transform, ratio=mkspan/mkspan[algorithm=="area"])
xbb$algName<-as.character(xbb$algorithm)

xbb[xbb$algorithm == "HetPrio" & xbb$stealLatest == "yes", "algName"] <- "HetPrioIndep"
xbb[xbb$algorithm == "zhang", "algName"] <- "OnlineZhang"
xbb[xbb$algorithm == "area", "algName"] <- "bound"
xbb[xbb$algorithm == "indep", "algName"] <- as.character(xbb[xbb$algorithm == "indep", "indep"])

# Keep the best result for all algorithm
xbb <- ddply(xbb[xbb$algName != "OnlineZhang",], ~input+algName+prob,summarize, ratio=min(ratio),time=time[which.min(ratio)])
# xbb <- ddply(xbb, ~input+algName+prob,summarize, ratio=min(ratio),time=time[which.min(ratio)])


xbbavg<-ddply(xbb, ~input+algName, summarize, ratio=mean(ratio), time = mean(time))

p<-ggplot(xbbavg[xbbavg$input <=48 & xbbavg$input > 4 & xbbavg$algName != "bound" & xbbavg$algName != "HetPrio", ], aes(x=input, y=ratio, color=algName, linetype=algName)) + geom_line()
p <- prepare_plot(p)
p <- p +ylab("Ratio of makespan to lower bound")
ggsave(paste(outdir, "All-Indep.pdf", sep=''),  p, height=4, width=6)
q
## nlogn <- function(x) { (0.01/23) * x * log(x) }

p<-ggplot(xbbavg[xbbavg$input <=48 & xbbavg$input > 4  & xbbavg$algName != "HetPrio", ], aes(x=input, y=time/1000, color=algName, linetype=algName)) + geom_line()
# p <- p + stat_function(fun = nlogn, color = "black")
p <- prepare_plot(p)
p <- p +ylab("Computation time (s, logscale)")
p <-  p + scale_y_log10()
ggsave(paste(outdir, "All-Indep-Time.pdf", sep=''),  p, height=4, width=6)
